## Application scenario
**Summary**

Create a small web application that ranks the words on a given wikipedia topic

**Details**
1. Create a web application with NodeJS + UI using any frontend framework you want
2. UI - create a search input for the search topic
3. NodeJS - Make a call to wikipedia API to get list of topics for the specific query using, the
   rest call below, for example for the query ‘NASA’
   https://en.wikipedia.org/w/api.php?action=opensearch&search=NASA
4. NodeJS - Take the first topic and get its contents by calling
   https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=NASA
   Extract the text from the response (from the “extract” property)
5. UI - in a table or list:
   * The text words (with ranks) sorted by occurence rank (descending order)
   * Ranks may have values from 1 to 5. Most common word has rank 5
   * Words with the same rank sorted alphanumerically by ascending order
   * Rank presented as number of stars - 5: *****, 4: ****, etc.
   * Note that we are not looking for the count of the words but word’s rank, meaning
   for example that the highest appearance in the text should get 5 stars

6. Make sure you have clean code and unit tests on NodeJS
   Example
   Input text: “car bicycle car bicycle car bicycle car bicycle car bicycle car bicycle plane plane
   truck”
   Output
   Bicycle (*****)
   Car (*****)
   Plane (**)
   Truck (*)

## How to run this project:

```sh
    tsc                  <= It will transpile ts based on a tsconfig.json
    npm install          <= install all the npm Dependencies
    npm start            <= It will transpile ts and run project on port 3000.
```

## Directory strcture of project:

* app.ts - Typescript file for creating express application class and where we have initialized the application.
* routes.ts - Typescript files for creating all the routes under Init() Method.
* package.json - Contains all the packages and dev dependencies required for this application. You can add more as your requirement.
* tsconfig.json - Where all the typescript configuration is there and we converting typescript into ES2018.
* Controller Folder - Contains all the classes for the controller of the express application.
* tsconfig.json - Contains all the rules for TypeScript linting.
