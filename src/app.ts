import dotenv from 'dotenv';
import express from 'express';
import ExpressRotuer from './express.router';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import nocache from 'nocache';

dotenv.config(); // ({ silent: true });

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(nocache());
app.set('view engine', 'ejs');
app.set('etag', false);
app.disable('view cache');

const expressRoutes = new ExpressRotuer(app);
expressRoutes.init();
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Express server app listening on port ${port}!`);
});
