import {NextFunction, Request, Response} from 'express';
import WikiService from '../services/wiki.service';
import Utils from '../common/utils';

export default class AppController {

    public static getDefault(req: Request, res: Response, next: NextFunction) {
        res.render('index', {year: new Date().getFullYear()});
    }

    public static async rankWordsWiki(req: Request, res: Response, next: NextFunction) {
        const searchWord = req.params.word;
        let retJson: object;
        try {
            const title = await WikiService.getWikiTitle(searchWord);
            const letterHtml = await WikiService.getWikiText(title);
            const letterText = Utils.cleanHtml(letterHtml);
            // original
            // const letterText = "car bicycle car bicycle car bicycle car bicycle car bicycle car bicycle plane plane truck";
            // all 5 options
            // const letterText = "car bicycle car bicycle car bicycle car bicycle car bicycle car bicycle bicycle bicycle plane plane truck trailer trailer trailer trailer";
            const letterWords = Utils.extractWords(letterText);
            retJson = {
                status: 'success',
                words: letterWords,
                text: letterText
            }
        } catch (err) {
            err.data = err.data || { method: 'AppController.rankWordsWiki' };
            retJson = {
                status: 'error',
                statusCode: err.statusCode || 500,
                message: err.message,
                data: err.data
            }
        }
        res.json(retJson);
    }

}
