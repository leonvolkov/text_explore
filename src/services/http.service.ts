import fetch, { Response } from 'node-fetch'

export default class HttpService {

    public static async get(url: string): Promise<Response> {
        const response: Response = await fetch(url);
        if (!response.ok) throw new Error(`unexpected response ${response.statusText}`);

        return response;
    }

    public static async getJson(listUrl: string): Promise<any> {
        let retJson: any;
        let result: any;
        try {
            result = await HttpService.get(listUrl);
        } catch (err) {
            err.data = err.data || { method: 'HttpService.getJson' };
            throw err;
        }
        try {
            retJson = await result.json();
        } catch (err) {
            err.data = { method: 'HttpService.getJson', result: result };
            throw err;
        }
        return retJson;
    }

}