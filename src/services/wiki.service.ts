import urlencode from "urlencode";
import HttpService from "./http.service";
import Utils from '../common/utils'

export default class WikiService {

    public static async getWikiTitle(searchWord: string): Promise<string> {
        const listUrl = `https://en.wikipedia.org/w/api.php?action=opensearch&search=${urlencode(searchWord)}`;
        let retJson: Array<any>;
        try {
            retJson = await HttpService.getJson(listUrl);
        } catch (err) {
            err.data = err.data || { method: 'WikiService.getWikiTitle' };
            throw err;
        }
        if (retJson[1] && retJson[1].length) {
            return retJson[1][0];
        } else {
            const err =  new Error(`no results for ${searchWord}`);
            throw err;
        }
    }

    public static async getWikiText(title: string): Promise<string> {
        const letterUrl = `https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=${urlencode(title)}`;
        let retHtml: string;
        try {
            const retJson = await HttpService.getJson(letterUrl);
            retHtml = Utils.findValueByKey('extract', retJson);
        } catch (err) {
            err.data = err.data || { method: 'WikiService.getWikiText' };
            throw err;
        }
        return retHtml;
    }



}