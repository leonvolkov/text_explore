import {Express, Router, static as Static} from 'express';
import AppController from './controller/app.controller';

export default class ExpressRouter {

    public router: Router;
    private app: Express;

    constructor(app: Express) {
        this.router = Router();
        this.app = app;
    }

    public init(): void {
        this.router.get('/', AppController.getDefault);
        this.router.get('/ranks_words/wikipedia/:word', AppController.rankWordsWiki);

        this.app.use('/', Static('public'));
        this.app.use('/', this.router);
    }

}
