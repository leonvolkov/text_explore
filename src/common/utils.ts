import urlencode from "urlencode";
import Filtering from '../common/filtering_constants'

export default class Utils {

    public static encodeJson(jsonObject: Object) {
        const str = JSON.stringify(jsonObject);
        return urlencode(str);
    }

    public static  findValueByKey(key: string, obj: any ): any {
        let value: any;
        Object.keys(obj).some((k) => {
            if (k === key) {
                value = obj[k];
                return true;
            }
            if (obj[k] && typeof obj[k] === 'object') {
                value = Utils.findValueByKey(key, obj[k]);
                return value !== undefined;
            }
        });
        return value;
    }

    public static cleanHtml(html: string): string {
        if(!html) return '';
        const text = html.replace(/(<([^>]+)>)/ig,"");
        return text;
    }

    public static extractWords(text: string): any {
        const wordCounts: any = { };
        const words = text.toLowerCase().trim().match(/\w+/g);
        if (words) {
            for(const word of words) {
                if (
                    word.length < 2 ||
                    Filtering.english.conjunctions.includes(word) ||
                    Filtering.english.determiners.includes(word) ||
                    Filtering.english.prepositions.includes(word) ||
                    Filtering.english.pronouns.includes(word)
                ) continue;
                wordCounts[word] = (wordCounts[word] || 0) + 1;
            }
        }
        let maxCount = 0;
        let minCount = Number.POSITIVE_INFINITY;
        const wordCountsArray = [];

        for(const [key, value] of Object.entries(wordCounts)) {
            if (typeof value === "number") {
                maxCount = Math.max(maxCount, value);
                minCount = Math.min(minCount, value);
            }
        }
        for(const [key, value] of Object.entries(wordCounts)) {
            const starPrice = (maxCount - minCount) / 4;
            if (typeof value === "number") {
                let rating: number;
                if (value === minCount) {
                    rating = 1;
                } else {
                    rating = Math.ceil((value - minCount) / starPrice) +1;
                }
                wordCountsArray.push(
                    {
                        word: key,
                        rating: rating
                    }
                );
            }
        }
        wordCountsArray.sort((a, b) => b.rating - a.rating || a.word.localeCompare(b.word));
        return wordCountsArray;
    }

}
