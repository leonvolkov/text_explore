class App {

    static init() {
        document.getElementById('searchForm').addEventListener('submit', App.getWordsData);
        App.removeLoaderEl();
    }

    static async getWordsData(event) {
        event.preventDefault();
        const keyword = document.getElementById('keyword').value;
        const url = `/ranks_words/wikipedia/${keyword}`;
        const listContainer = document.getElementById('wordsList');
        listContainer.innerHTML = '';
        App.showLoaderEl();
        try {
            const response = await fetch(url);
            const wordsList = await response.json();
            console.dir(wordsList);
            const list = document.createElement('OL');
            listContainer.appendChild(list);
            if (wordsList.words && wordsList.words.length > 0) {
                for(const entry of wordsList.words) {
                    const li = document.createElement('LI');
                    li.textContent = `${entry.word} (${'*'.repeat(entry.rating)})`;
                    list.appendChild(li);
                }
            } else {
                const li = document.createElement('LI');
                li.textContent = 'There is no results.'
                list.appendChild(li);
            }
        } catch(error) {
            console.log(error);
        };

        App.removeLoaderEl();
    }

    static showLoaderEl() {
        const loader = document.getElementById("loader");
        loader.classList.remove("removed");
    }

    static removeLoaderEl() {
        const loader = document.getElementById("loader");
        loader.classList.add("removed");
    }

}


window.addEventListener('DOMContentLoaded', () => {
    App.init();
});